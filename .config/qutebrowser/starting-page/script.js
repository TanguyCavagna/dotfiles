// DOM pointers
const DOMTime = document.querySelector('#time');
const DOMWeather = document.querySelector('#weather');

// Helper functions
const getTime = () => {
    const DT = moment().tz('Europe/Zurich');
    return DT.format('HH:mm:ss');
};

const K2C = (K) => {
    return Math.floor(K - 273.15);
};

// Deamons
setInterval(() => {
    DOMTime.textContent = getTime();
}, 1000);

setTimeout(() => {
    DOMTime.textContent = getTime();
}, 0);

// Data fetching
fetch(
    'https://api.openweathermap.org/data/2.5/weather?lat=46.1908&lon=6.0405&appid=5a80161e1c21729c204e3adda345ad5e&exclude=hourly,daily',
    {
        method: 'GET',
    }
)
    .then((res) => res.json())
    .then((data) => {
        DOMWeather.querySelector('#desc').textContent = `${
            data.weather[0].description
        } - ${K2C(data.main.temp)}°C`;
        DOMWeather.querySelector(
            '#icon'
        ).src = `https://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`;
    });
