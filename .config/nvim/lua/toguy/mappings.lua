local function map(mode, lhs, rhs, opts)
    local options = { noremap = true }
    if opts then options = vim.tbl_extend('force', options, opts) end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

map('n', '<C-b>', '<cmd>Lexplore<cr>')
map('n', '<leader>lf', '<cmd>lua vim.lsp.format()<cr>')
map('n', '<C-H>', '<C-W><C-H>')
map('n', '<C-J>', '<C-W><C-J>')
map('n', '<C-K>', '<C-W><C-K>')
map('n', '<C-L>', '<C-W><C-L>')
map('t', '<C-H>', '<C-\\><C-n><C-W><C-H>')
map('t', '<C-J>', '<C-\\><C-n><C-W><C-J>')
map('t', '<C-K>', '<C-\\><C-n><C-W><C-K>')
map('t', '<C-L>', '<C-\\><C-n><C-W><C-L>')
map('n', '<A-k>', '[e', { noremap = false })
map('n', '<A-j>', ']e', { noremap = false })
map('v', '<A-k>', '[egv', { noremap = false })
map('v', '<A-j>', ']egv', { noremap = false })

-- Telescope

map('n', '<leader>ff', '<cmd>Telescope find_files<cr>')
map('n', '<leader>ff', '<cmd>Telescope live_grep<cr>')

-- Terminal

map('n', '<leader>tf', '<cmd>FloatermToggle<cr>')
map('n', '<leader>gg', '<cmd>FloatermNew --width=0.8 --height=0.8 --autoclose=2 lazygit<cr>')

-- Symbols outline

map('n', '<leader>so', '<cmd>TagbarToggle<cr>')

-- Synbols rename

map('n', '<leader>rr', '<cmd>lua vim.lsp.buf.rename()<cr>')
