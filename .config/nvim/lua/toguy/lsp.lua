local lsp = require('lspconfig')

local on_attach = function(client, bufnr)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer=bufnr })
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, { buffer=bufnr})
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer=bufnr})
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, { buffer=bufnr})
    vim.keymap.set('n', 'gT', vim.lsp.buf.type_definition, { buffer=bufnr})
    vim.keymap.set('n', 'gr', '<cmd>Telescope lsp_references<cr>', { buffer=bufnr})
    vim.keymap.set('n', '<leader>dj', vim.diagnostic.goto_next, { buffer=bufnr})
    vim.keymap.set('n', '<leader>dk', vim.diagnostic.goto_prev, { buffer=bufnr})
    vim.keymap.set('n', '<leader>dl', "<cmd>Telescope diagnostics<cr>", { buffer=bufnr})
    vim.keymap.set('n', '<leader>r', vim.lsp.buf.rename, { buffer=bufnr})
    vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, { buffer=bufnr})
end

local capabilities = require('cmp_nvim_lsp').default_capabilities()
local servers = { 'pyright', 'clangd', 'jdtls' }
for _, serv in ipairs(servers) do
    lsp[serv].setup {
        on_attach = on_attach,
        capabilities = capabilities
    }
end
