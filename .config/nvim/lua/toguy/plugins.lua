return require('packer').startup(function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- Telescope
    use {
        'nvim-telescope/telescope.nvim', tag = '0.1.0',
        requires = { {'nvim-lua/plenary.nvim'} }
    }
    
    -- Additional plugins recommended for telescope
    use {'nvim-telescope/telescope-fzf-native.nvim', run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }
    
    -- Lualine
    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'kyazdani42/nvim-web-devicons', opt = true }
    }

    -- Gitsigns
    use 'lewis6991/gitsigns.nvim'

    -- Mason
    use {
        'williamboman/mason.nvim',
        'williamboman/mason-lspconfig.nvim',
        'neovim/nvim-lspconfig',
    }

    -- Autocomplete
    use 'hrsh7th/nvim-cmp'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'

    -- Formatter
    use 'jose-elias-alvarez/null-ls.nvim'

    -- Snippet
    use { 'L3MON4D3/LuaSnip', tag = 'v<CurrentMajor>.*' }

    -- Treesitter
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    }

    -- Surround
    use({
        'kylechui/nvim-surround',
        tag = "*", -- Use for stability; omit to use `main` branch for the latest features
        config = function()
            require('nvim-surround').setup({
                -- Configuration here, or leave empty to use defaults
            })
        end
    })

    -- Floaterm
    use 'voldikss/vim-floaterm'

    -- Commenter
    use { 'terrortylor/nvim-comment', config = function() require('nvim_comment').setup() end }

    -- Autopairs
    use { 'windwp/nvim-autopairs', config = function() require('nvim-autopairs').setup {} end }

    -- Symbols outline
    use 'preservim/tagbar'

    -- Line movements
    use 'tpope/vim-unimpaired'

end)
