-- GENERAL CONFIG

vim.g.mapleader = ' '

local set = vim.opt

set.termguicolors = true

-- number of space in a <tab>
set.tabstop = 4
set.softtabstop = 4
set.expandtab = true

-- enable autoindent
set.smartindent = true

-- number of spaces for autoindent
set.shiftwidth = 4

-- adds line number
set.number = true
set.relativenumber = true
set.signcolumn = 'auto'
-- column width for the line number
set.numberwidth = 4

-- highlights the matched text pattern on search
set.incsearch = true
set.hlsearch = false

-- open splits intuitively
set.splitbelow = true
set.splitright = true

-- navigate buffers without losing unsaved work
set.hidden = true

-- start scrolling when 8 lines from frop top/bottom
set.scrolloff = 8

-- enable mouse support
set.mouse = 'a'

-- case insensitive search unless capital letters are used
set.ignorecase = true
set.smartcase = true

-- hide mode
set.showmode = false

vim.cmd('colorscheme jellybeans')

-- PLUGIN CONFIGS

require('toguy')
