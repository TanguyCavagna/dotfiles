# Toguy's dotfiles

# What are dotfiles ?
![unixporn](https://gitlab.com/TanguyCavagna/dotfiles/-/raw/master/.screenshots/dotfiles01.png)
Dotfiles are the customization files that are used to personalize your Linux or other Unix-based system.  You can tell that a file is a dotfile because the name of the file will begin with a period–a dot!  The period at the beginning of a filename or directory name indicates that it is a hidden file or directory.  This repository contains my personal dotfiles.  They are stored here for convenience so that I may quickly access them on new machines or new installs.  Also, others may find some of my configurations helpful in customizing their own dotfiles.

# Fonts
Any Nerd Font is required. I suggest the [FiraMono Nerd Font](https://www.nerdfonts.com/font-downloads) one.

# My i3wm & polybar config
* [i3-gaps](https://gitlab.com/TanguyCavagna/dotfiles/-/tree/master/.config/i3)
* [polybar](https://gitlab.com/TanguyCavagna/dotfiles/-/tree/master/.config/polybar)

# Required packages
Some packages are required by my dotfiles. Those can be found in the [.packages](https://gitlab.com/TanguyCavagna/dotfiles/-/tree/master/.packages) directory.
